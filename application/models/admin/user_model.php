<?php
class User_model extends CI_Model {

	public function __construct()	{
		$this->load->database();
	}
	
	// Menampilkand data user
	public function daftar_user() {
		$query = $this->db->query('SELECT users.nama, users.email, users.username, users.password, users.id_user FROM users');
		return $query->result_array();
	}
	
	// Model untuk menambah user
	public function tambah($data) {
		return $this->db->insert('users', $data);
	}
	
	// Detail user
	public function detail_user($id = FALSE) {
	if ($id === FALSE)	{
		$query = $this->db->get('users');
		return $query->result_array();
	}
	$query = $this->db->get_where('users', array('id_user' => $id));
	return $query->row_array();
	}
	
	// Update user
	public function edit_user($data) {
		$this->db->where('id_user', $data['id_user']);
		return $this->db->update('users', $data);
	}
	
	// Hapus user
	public function delete_user($id) {
		$this->db->where('id_user',$id);
		return $this->db->delete('users');
	}
}