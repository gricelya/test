<header>
	<div id="logo"><a href="<?php echo base_url(); ?>"><img src="<?php echo base_url(); ?>assets/images/UB.jpg" width="400" height="400"></a></div>
	<div id="nama"><span class="nama">Hot News Brawijaya</span><br>
</header>

<nav>
  	<ul id="MenuBar1" class="MenuBarHorizontal">
    	<li><a href="<?php echo base_url(); ?>home">Home</a></li>        
		<li><a href="<?php echo base_url(); ?>kategori/ub">Berita UB</a></li>        
		<li><a class="MenuBarItemSubmenu">Berita Fakultas</a>
			<ul>
				<li><a href="<?php echo base_url(); ?>kategori/filkom">FILKOM</a></li>        
				<li><a href="<?php echo base_url(); ?>kategori/fia">FIA</a></li>   
				<li><a href="<?php echo base_url(); ?>kategori/fpik">FPIK</a></li> 
			</ul>
		</li>		
		<li><a href="<?php echo base_url(); ?>about_us">About Us</a></li>        
        <li><a href="<?php echo base_url(); ?>kontak">Kontak</a></li>
		<li><a href="<?php echo base_url(); ?>admin/login">Login</a></li>
    </ul>
</nav>
<script type="text/javascript">
	var MenuBar1 = new Spry.Widget.MenuBar("MenuBar1", {imgDown:"<?php echo base_url(); ?>assets/SpryAssets/SpryMenuBarDownHover.gif", imgRight:"<?php echo base_url(); ?>assets/SpryAssets/SpryMenuBarRightHover.gif"});
</script> 