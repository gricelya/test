<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class User extends CI_Controller {
	
	public function __construct()	{
		parent::__construct();
		$this->load->model('admin/user_model');
		$this->load->library('form_validation');
	}
	
	public function index() {
		$query = $this->user_model->daftar_user();
		$data=array('title'=>'Kelola User',
					'user' => $query,
					'isi'  =>'admin/user/user_view'
						);
		$this->load->view('admin/layout/wrapper',$data);	
	}
	
	// Controler tambah user di sini
	public function tambah() {
		$this->form_validation->set_rules('nama', 'Nama', 'required');
		$this->form_validation->set_rules('email', 'Email', 'required');
		$this->form_validation->set_rules('username', 'Username', 'required');
		$this->form_validation->set_rules('password', 'Password', 'required');
		if ($this->form_validation->run() === FALSE) {
			$data=array('title'=>'Menambah user',
						'isi'  =>'admin/user/tambah_user'					
							);
			$this->load->view('admin/layout/wrapper',$data);	
		}else{		
			$data = array(
						'nama' 		=> $this->input->post('nama'),					
						'email'		=> $this->input->post('email'),
						'username'	=> $this->input->post('username'),
						'password'	=> $this->input->post('password')
					);
			$this->user_model->tambah($data);
			redirect(base_url().'admin/user/');
		}
	}
	
	// Menampilkan halaman edit
	public function edit($id) {	
		$this->form_validation->set_rules('nama', 'Nama', 'required');
		$this->form_validation->set_rules('email', 'Email', 'required');
		$this->form_validation->set_rules('username', 'Username', 'required');
		$this->form_validation->set_rules('password', 'Password', 'required');
		if ($this->form_validation->run() === FALSE) {
		$data['user'] = $this->user_model->detail_user();
		$data['detail']	= $this->user_model->detail_user($id);
		$data=array('title'		=> 'Mengubah user: '.$data['detail']['nama'],
					'user'		=> $this->user_model->detail_user(),
					'detail' 	=> $this->user_model->detail_user($id),
					'isi'  		=>'admin/user/edit_user'
						);
		$this->load->view('admin/layout/wrapper',$data);
		// Kalau tidak ada error user diupdate
		}else{			
			$data = array(
					'id_user'	=> $this->input->post('id_user'),
					'nama'	 	=> $this->input->post('nama'),
					'email'		=> $this->input->post('email'),
					'username' 	=> $this->input->post('username'),
					'password'	=> $this->input->post('password')
				);
			$this->user_model->edit_user($data);
			redirect(base_url().'admin/user/');
		}
	}
	
	// Menghapus user
	public function delete($id) {
		$this->user_model->delete_user($id);
		redirect(base_url().'admin/user/');
	}
}