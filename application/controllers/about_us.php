<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class About_Us extends CI_Controller {
	public function index() {
		$data=array('title'=>'Contact Us',
					'isi'  =>'home/about_us_view'
						);
		$this->load->view('layout/wrapper',$data);	
	}
}