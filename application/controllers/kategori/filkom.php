<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Filkom extends CI_Controller {
	
	public function __construct()	{
		parent::__construct();
		$this->load->model('kategori/berita_model_filkom');
	}
	
	public function index() {
		$data=array('title'		=>'Berita FILKOM',
					'berita'	=> $this->berita_model_filkom->daftar_berita(),
					'isi'  		=>'home/index_home'
						);
		$this->load->view('layout/wrapper',$data);	
	}
	
	// Read berita
	public function read($read) {
		$data['berita'] = $this->berita_model_filkom->daftar_berita();
		$data['detail']	= $this->berita_model_filkom->daftar_berita($read);
		$data=array('title'		=>$data['detail']['judul'],
					'berita'	=> $this->berita_model_filkom->daftar_berita(),
					'detail' 	=> $this->berita_model_filkom->daftar_berita($read),
					'isi'  		=>'home/read_view'
						);
		$this->load->view('layout/wrapper',$data);	
		
	}
}